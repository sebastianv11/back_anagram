<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Word;
use Illuminate\Support\Facades\DB;

class WordController extends Controller
{
    public function getData()
    {
        return DB::table('words')->get();
    }
    public function store(Request $request)
    {
        $word = new Word;
        $word->word1 = $request->word1;
        $word->word2 = $request->word2;
        $word->anagram = $request->anagram;
        $word->save();
        return $word;
    }
}
